/* 2019 David DiPaola, licensed CC0 (public domain worldwide) */

#include <stdio.h>

#include <SDL.h>

struct sdl {
	SDL_Window   * window;
	SDL_Renderer * renderer;
};

static const int _width  = 320;
static const int _height = 240;

static int
_init(struct sdl * sdl) {
	SDL_Init(SDL_INIT_VIDEO);

	sdl->window = SDL_CreateWindow(
		/*title=*/ "window title",
		/*x=*/ SDL_WINDOWPOS_CENTERED,
		/*y=*/ SDL_WINDOWPOS_CENTERED,
		/*w=*/ _width,
		/*h=*/ _height,
		/*flags=*/ 0
	);
	if (!sdl->window) {
		printf("ERROR: SDL_CreateWindow() failed: %s" "\n", SDL_GetError());
		return -1;
	}

	sdl->renderer = SDL_CreateRenderer(
		/*window=*/ sdl->window,
		/*index=*/ -1,
		/*flags=*/ SDL_RENDERER_ACCELERATED
	);
	if (!sdl->renderer) {
		printf("ERROR: SDL_CreateRenderer() failed: %s" "\n", SDL_GetError());
		return -2;
	}
	SDL_RenderClear(sdl->renderer);

	return 0;
}

static void
_close(struct sdl * sdl) {
	SDL_DestroyRenderer(sdl->renderer);
	sdl->renderer = NULL;

	SDL_DestroyWindow(sdl->window);
	sdl->window = NULL;

	SDL_Quit();
}

static int
_fillred(struct sdl * sdl) {
	int status;

	SDL_Surface * surface = SDL_CreateRGBSurfaceWithFormat(
		/*flags=*/ 0,
		/*width=*/ _width,
		/*height=*/ _height,
		/*depth=*/ 32,
		/*format=*/ SDL_PIXELFORMAT_ARGB8888
	);
	if (!surface) {
		printf("ERROR: SDL_CreateRGBSurfaceWithFormat() failed: %s" "\n", SDL_GetError());
		return -1;
	}

	status = SDL_FillRect(
		/*dst=*/ surface,
		/*rect=*/ NULL,
		/*color=*/ 0xFFFF0000
	);
	if (status != 0) {
		printf("ERROR: SDL_FillRect() failed: %s" "\n", SDL_GetError());
		return -2;
	}

	SDL_Texture * texture = SDL_CreateTextureFromSurface(
		/*renderer=*/ sdl->renderer,
		/*surface=*/ surface
	);
	if (!texture) {
		printf("ERROR: SDL_CreateTextureFromSurface() failed: %s" "\n", SDL_GetError());
		return -3;
	}

	SDL_FreeSurface(surface);
	surface = NULL;

	status = SDL_RenderCopy(
		/*renderer=*/ sdl->renderer,
		/*texture=*/ texture,
		/*srcrect=*/ NULL,
		/*dstrect=*/ NULL
	);
	if (status != 0) {
		printf("ERROR: SDL_RenderCopy() failed: %s" "\n", SDL_GetError());
		return -4;
	}

	SDL_DestroyTexture(texture);

	return 0;
}

int
main() {
	struct sdl sdl;
	int status;

	status = _init(&sdl);
	if (status < 0) {
		return 1;
	}

	int quit = 0;
	while (!quit) {
		SDL_RenderClear(sdl.renderer);
		status = _fillred(&sdl);
		if (status < 0) {
			return 2;
		}
		SDL_RenderPresent(sdl.renderer);

		SDL_Delay(16);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				quit = 1;
				break;
			}
		}
	}

	_close(&sdl);
	return 0;
}

