# 2019 David DiPaola, licensed CC0 (public domain worldwide)

SRC_C = main.c
BIN = sdl_helloworld

_CFLAGS = \
	-std=c99 -fwrapv \
	-Wall -Wextra \
	-g \
	-O2 \
	$(shell pkg-config --cflags sdl2) \
	$(CFLAGS)
_LDFLAGS = \
	-O1 \
	$(LDFLAGS)
_LDFLAGS_LIB = \
	$(shell pkg-config --libs sdl2)

OBJ = $(SRC_C:.c=.o)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -rf $(OBJ) $(BIN)

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) $^ -o $@ $(_LDFLAGS_LIB)

